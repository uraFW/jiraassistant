﻿unit uVarObj;

interface

uses
  System.SysUtils, Data.DB;

type
  TuForEachProc = reference to procedure (ANameOrIndex, AValue: Variant; var ABreak: Boolean);

function CreateObject(AParams: TParams = nil): Variant;
function ParamsOf(AObject: Variant): TParams;
function IsValidObject(AObject: Variant): Boolean;
function CountElements(AObjectOrArray: Variant): Integer;
function ForEach(AObjectOrArray: Variant; AProc: TuForEachProc): Integer;
function DumpObject(AObject: Variant): string;

implementation

uses Winapi.Windows, Winapi.ActiveX, System.Variants, System.Types, System.Classes, System.Win.ComObj,
  System.VarUtils;

type
  TCustomVariantObjectClass = class of TCustomVariantObject;
  TCustomVariantObject = class(TInterfacedObject, IDispatch)
  private // IDispatch interface implementation
    function GetTypeInfoCount(out Count: Integer): HResult; stdcall;
    function GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult; stdcall;
    function GetIDsOfNames(const IID: TGUID; ANames: Pointer; NameCount, LocaleID: Integer; ADispIDs: Pointer): HResult; stdcall;
    function Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer; Flags: Word; var AParams; AVarResult, AExcepInfo, ArgErr: Pointer):
      HResult; stdcall;
  protected // This methods must be override in inherited classes
    FHResult: HResult;
    function GetPropID(APropName: string): Integer; virtual; abstract;
    function GetProp(DispID: Integer; AParams: Variant): Variant; virtual; abstract;
    procedure SetProp(DispID: Integer; AParams: Variant; AValue: Variant); virtual; abstract;
    function InvokeMethod(DispID: Integer; AParams: Variant): Variant; virtual; abstract;
  end;

  IVarObjParams = interface
    ['{1D6EE4F2-0CCB-4B5B-BC33-9D3A97568952}']
    function GetParams: TParams;
  end;

  TuVariantObject = class(TCustomVariantObject, IVarObjParams)
  private // IVarObjParams
    FParams: TParams;
    function GetParams: TParams;
    constructor Create(AParams: TParams);
    procedure InternalSetProperty(AParams: Variant);
  protected
    function GetPropID(APropName: string): Integer; override;
    function GetProp(DispID: Integer; AParams: Variant): Variant; override;
    procedure SetProp(DispID: Integer; AParams: Variant; AValue: Variant); override;
    function InvokeMethod(DispID: Integer; AParams: Variant): Variant; override;
  end;

  TsblVariantObjectWithOwnParams = class(TuVariantObject)
  private
    constructor Create;
  public
    destructor Destroy; override;
  end;

function CreateObject(AParams: TParams): Variant;
begin
  if AParams = nil then
    Result := IDispatch(TsblVariantObjectWithOwnParams.Create)
  else
    Result := IDispatch(TuVariantObject.Create(AParams));
end;

function ParamsOf(AObject: Variant): TParams;
var
  vd: TVarData;
  iParams: IVarObjParams;
begin
  Result := nil;
  if VarIsType(AObject, varDispatch) then
  begin
    vd := TVarData(AObject);
    if Supports(IDispatch(vd.VDispatch), IVarObjParams, iParams) then
      Result := iParams.GetParams;
  end;
end;

function IsValidObject(AObject: Variant): Boolean;
var
  vd: TVarData;
begin
  Result := False;
  if VarIsType(AObject, varDispatch) then
  begin
    vd := TVarData(AObject);
    Result := Supports(IDispatch(vd.VDispatch), IVarObjParams);
  end;
end;

function CountElements(AObjectOrArray: Variant): Integer;
begin
  Result := 0;
  if IsValidObject(AObjectOrArray) then
    Result := ParamsOf(AObjectOrArray).Count
  else if VarIsArray(AObjectOrArray) then
    Result := VarArrayHighBound(AObjectOrArray, 1) + 1;
end;

function ForEach(AObjectOrArray: Variant; AProc: TuForEachProc): Integer;
var ObjParams: TParams; i: Integer; bBreak: Boolean;
begin
  bBreak := False; Result := 0;
  if IsValidObject(AObjectOrArray) then
  begin
    ObjParams := ParamsOf(AObjectOrArray);
    for i := 0 to ObjParams.Count - 1 do
    begin
      AProc(ObjParams[i].Name, ObjParams[i].Value, bBreak);
      Inc(Result);
      if bBreak then break;
    end;
  end
  else if VarIsArray(AObjectOrArray) then
  begin
    for i := VarArrayLowBound(AObjectOrArray, 1) to VarArrayHighBound(AObjectOrArray, 1) do
    begin
      AProc(i, AObjectOrArray[i], bBreak);
      Inc(Result);
      if bBreak then break;
    end;
  end;
end;

function InternalValueToStr(v: Variant): string;
begin
  if VarIsNull(v) then Result := 'null'
  else Result := VarToStr(v);
  if VarIsStr(v) then
    Result := AnsiQuotedStr(Result,'"');
end;

procedure InternalDumpObject(AObject: Variant; ADumpLines: TStrings; AParentLevel: Integer); forward;

procedure InternalDumpArray(AArray: Variant; ADumpLines: TStrings; AParentLevel: Integer);
var i,c: Integer; v: Variant; sPrefix: string;
begin
  c := VarArrayHighBound(AArray, 1) + 1;
  sPrefix:=StringOfChar(' ',AParentLevel);
  ADumpLines.Add(sPrefix+'array('+IntToStr(c)+') {');
  for i := VarArrayLowBound(AArray, 1) to VarArrayHighBound(AArray, 1) do
  begin
    v:=AArray[i];
    if IsValidObject(v) then
    begin
      ADumpLines.Add(sPrefix+'['+IntToStr(i)+'] =');
      InternalDumpObject(v, ADumpLines, AParentLevel+1);
    end
    else if VarIsArray(v) then
    begin
      ADumpLines.Add(sPrefix+'['+IntToStr(i)+'] =');
      InternalDumpArray(v, ADumpLines, AParentLevel+1);
    end
    else ADumpLines.Add(sPrefix+'['+IntToStr(i)+'] = '+InternalValueToStr(v));
  end;
  ADumpLines.Add(sPrefix+'}');
end;

procedure InternalDumpObject(AObject: Variant; ADumpLines: TStrings; AParentLevel: Integer);
var ObjParams: TParams; i: Integer; v: Variant; sPrefix: string;
begin
  sPrefix:=StringOfChar(' ',AParentLevel);
  ADumpLines.Add(sPrefix+'{');
  ObjParams := ParamsOf(AObject);
  if Assigned(ObjParams) then
    for i:=0 to ObjParams.Count-1 do
    begin
      v:=ObjParams[i].Value;
      if IsValidObject(v) then
      begin
        ADumpLines.Add(sPrefix+'"'+ObjParams[i].Name+'" =');
        InternalDumpObject(v, ADumpLines, AParentLevel+1);
      end
      else if VarIsArray(v) then
      begin
        ADumpLines.Add(sPrefix+'"'+ObjParams[i].Name+'" =');
        InternalDumpArray(v, ADumpLines, AParentLevel+1);
      end
      else ADumpLines.Add(sPrefix+'"'+ObjParams[i].Name+'" = '+InternalValueToStr(v));
    end;
  ADumpLines.Add(sPrefix+'}');
end;

function DumpObject(AObject: Variant): string;
var DumpLines: TStrings;
begin
  DumpLines := TStringList.Create;
  try
    if VarIsArray(AObject) then
      InternalDumpArray(AObject, DumpLines, 1)
    else
      InternalDumpObject(AObject, DumpLines, 1);
    Result := DumpLines.Text;
  finally
    DumpLines.Free;
  end;
end;

{ TCustomVariantObject }

function TCustomVariantObject.GetTypeInfoCount(out Count: Integer): HResult;
begin
  Count := 0;
  Result := S_OK;
end;

function TCustomVariantObject.GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult;
begin
  ITypeInfo(TypeInfo) := nil;
  Result := S_OK;
end;

function TCustomVariantObject.GetIDsOfNames;
var
  i: Integer;
begin
  for i := 0 to NameCount - 1 do
    PDispIDList(ADispIDs)[i] := GetPropID(POleStrList(ANames)[i]);
  Result := S_OK;
end;

function DispParamsToVarArray(AParams: TDispParams): Variant;
var
  i, j, c: Integer;
  ptr: PVariant;
begin
  VarClear(Result);
  c := AParams.cArgs - AParams.cNamedArgs;
  if c < 1 then
    Exit;
  Result := VarArrayCreate([0, c - 1], varVariant);
  ptr := PVariant(@AParams.rgvarg[AParams.cNamedArgs]);
  j := c - 1;
  for i := 0 to c - 1 do
  begin
    Result[j] := ptr^;
    Dec(j);
    Inc(ptr);
  end;
end;

function TCustomVariantObject.Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer; Flags: Word; var AParams;
  AVarResult, AExcepInfo, ArgErr: Pointer): HResult;
var
  Params: TDispParams;
  ExcepInfo: PExcepInfo;
  VarResult: Variant;
begin
  FHResult := S_OK;
  VarClear(VarResult);
  Params := TDispParams(AParams);
  try
    case Flags of
      DISPATCH_PROPERTYGET, DISPATCH_PROPERTYGET + DISPATCH_METHOD: VarResult := GetProp(DispID, DispParamsToVarArray(Params));
      DISPATCH_PROPERTYPUT: SetProp(DispID, DispParamsToVarArray(Params), PVariant(AParams)^);
      DISPATCH_METHOD: VarResult := InvokeMethod(DispID, DispParamsToVarArray(Params));
      DISPATCH_PROPERTYPUTREF: SetProp(DispID, DispParamsToVarArray(Params), PVariant(AParams)^);
    end;
    if Assigned(AVarResult) then
      PVariant(AVarResult)^ := VarResult;
    Result := FHResult;
  except
    on E: Exception do
    begin
      ExcepInfo := PExcepInfo(AExcepInfo);
      ZeroMemory(ExcepInfo, sizeof(TExcepInfo));
      ExcepInfo.wCode := 20000;
      ExcepInfo.bstrDescription := E.Message;
      Result := DISP_E_EXCEPTION;
    end;
  end;
end;

{ TuVariantObject }

constructor TuVariantObject.Create(AParams: TParams);
begin
  FParams := AParams;
end;

function TuVariantObject.GetParams: TParams;
begin
  Result := FParams;
end;

function TuVariantObject.GetPropID(APropName: string): Integer;
var
  p: TParam;
begin
  if APropName = '__SetProp' then exit(1);
  p := FParams.FindParam(APropName);
  if p = nil then
  begin
    p := TParam(FParams.Add);
    p.Name := APropName;
  end;
  Result := 100 + p.ID;
end;

function IndicesToBounds(AIndices: TIntegerDynArray): TIntegerDynArray;
var
  i, k, c: Integer;
begin
  c := Length(AIndices);
  SetLength(Result, c * 2);
  k := 0;
  for i := 0 to c - 1 do
  begin
    Result[k] := 0;
    Result[k + 1] := AIndices[i];
    Inc(k, 2);
  end;
end;

procedure CorrectIndexedProperty(p: TParam; Indices: TIntegerDynArray);
var v: Variant;
begin
  if not VarIsArray(p.Value) or (VarArrayDimCount(p.Value) <> Length(Indices)) then
    p.Value := VarArrayCreate(IndicesToBounds(Indices), varVariant)
  else if VarArrayHighBound(p.Value,1)<Indices[0] then
  begin
    v:=p.Value;
    VarArrayRedim(v, Indices[0]);
    p.Value:=v;
  end;
end;

function TuVariantObject.GetProp(DispID: Integer; AParams: Variant): Variant;
var
  p: TParam;
  Indices: TIntegerDynArray;
begin
  VarClear(Result);
  Indices := nil;
  p := TParam(FParams.FindItemID(DispID - 100));
  if Assigned(p) then
  begin
    if not VarIsArray(AParams) then
    begin
      Result := p.Value;
      Exit;
    end;
    Indices := AParams; // check for valid index array (all elements must be integer) !!!
    CorrectIndexedProperty(p, Indices);
    Result := VarArrayGet(p.Value, Indices);
  end;
end;

procedure TuVariantObject.SetProp(DispID: Integer; AParams: Variant; AValue: Variant);
var
  p: TParam;
  Indices: TIntegerDynArray;
  v: Variant;
begin
  if DispID = 1 then
  begin
    InternalSetProperty(AParams);
    exit;
  end;
  Indices := nil;
  p := TParam(FParams.FindItemID(DispID - 100));
  if Assigned(p) then
  begin
    if not VarIsArray(AParams) then
    begin
      p.Value := AValue;
      Exit;
    end;
    Indices := AParams; // check for valid index array (all elements must be integer) !!!
    CorrectIndexedProperty(p, Indices);
    v := p.Value;
    VarArrayPut(v, AValue, Indices);
    p.Value := v;
  end;
end;

procedure TuVariantObject.InternalSetProperty(AParams: Variant);
var PropName: string; PropValue, varIndices: Variant; i: Integer; Indices: TIntegerDynArray;
begin
  i := VarArrayHighBound(AParams, 1) + 1; //  i:=Length(AParams);
  if i<2 then exit;
  PropName:=VarToStr(AParams[0]);
  PropValue:=AParams[1];
  Indices := nil;
  varIndices := Unassigned;
  if i>2 then
  begin
    SetLength(Indices,1);
    Indices[0]:=AParams[2];
    varIndices := Indices;
  end;
  SetProp(GetPropID(PropName), varIndices, PropValue);
end;

function TuVariantObject.InvokeMethod(DispID: Integer; AParams: Variant): Variant;
begin
  case DispID of
    1: InternalSetProperty(AParams);
    else raise Exception.Create('Methods not supported for this object');
  end;
end;

{ TsblVariantObjectWithOwnParams }

constructor TsblVariantObjectWithOwnParams.Create;
begin
  FParams := TParams.Create;
end;

destructor TsblVariantObjectWithOwnParams.Destroy;
begin
  FParams.Free;
  inherited;
end;

end.

