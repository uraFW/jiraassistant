unit ja.JIRAAssistantController;

interface

uses
  SysUtils, Variants, Classes, Graphics, Controls, Forms, DB, System.RegularExpressions,
  IPPeerClient, REST.Client, REST.Authenticator.Basic, REST.Response.Adapter, Data.Bind.Components, Data.Bind.ObjectScope,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, REST.Types, REST.Authenticator.OAuth, FireDAC.Stan.StorageBin;

type
  TPeriod = packed record
    FromDate: TDateTime;
    ToDate: TDateTime;
    PeriodName: string;
    property Text: string read PeriodName;
  end;

  TWorklogTotals = packed record
    Days: double;
    Hours: double;
    Worklogs: Cardinal;
  end;

  TdmJIRAAssistantController = class(TDataModule)
    JIRA: TRESTClient;
    Auth: THTTPBasicAuthenticator;
    RequestProject: TRESTRequest;
    ResponseProject: TRESTResponse;
    RequestSearch: TRESTRequest;
    ResponseSearch: TRESTResponse;
    memtblWorklogs: TFDMemTable;
    dtfMyWorklogs_started: TDateTimeField;
    sfldMyWorklogs_key: TStringField;
    sfldMyWorklogs_comment: TStringField;
    dsWorklogs: TDataSource;
    ffldMyWorklogs_timespent: TFloatField;
    sfldMyWorklogsstatus: TStringField;
    memtblStaff: TFDMemTable;
    dsStaff: TDataSource;
    memtblEmployee: TFDMemTable;
    ifldStaffID: TIntegerField;
    sfldStaffName: TStringField;
    sfldStaffEmail: TStringField;
    ifldEmployeeID: TIntegerField;
    sfldEmployeeName: TStringField;
    sfldEmployeeEmail: TStringField;
    dsEmployee: TDataSource;
    ifldWorklogsEventType: TIntegerField;
    ifldWorklogsEventOptions: TIntegerField;
    ifldWorklogsID: TIntegerField;
    sfldWorklogsEventCaption: TStringField;
    ifldWorklogsEventColor: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
  private
    FFS: TFormatSettings;
    FWorklogTotals: TWorklogTotals;
    function InternalFormatDateTime(v: Variant): TDateTime;
    function MatchEvaluator(const Match: TMatch): string;
  public // JIRA requests
    function Login(ALogin, APassword: string): Boolean;
    function BuildPeriodList: TArray<TPeriod>;
    procedure UpdateWorklogs(AFromDate, AToDate: TDateTime);
    function SaveWorklogToHTML: string;
    procedure SaveWorklogToFile(AFile, APeriodName: string);
    function IsStatusDone(AStatus: string): Boolean;
    property WorklogTotals: TWorklogTotals read FWorklogTotals;
  end;

var
  dmJIRAAssistantController: TdmJIRAAssistantController;

implementation

uses
  System.Rtti, Types, DateUtils, Math, StrUtils, Generics.Collections,
  uJSON, uVarObj;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmJIRAAssistantController }

procedure TdmJIRAAssistantController.DataModuleCreate(Sender: TObject);
begin
  FFS := FormatSettings;
  FFS.DateSeparator := '-';
  FFS.ShortDateFormat := 'yyyy-mm-dd';
end;

function TdmJIRAAssistantController.InternalFormatDateTime(v: Variant): TDateTime;
var
  s: string;
begin
  s := VarToStr(v).Split(['T'])[0];
  if s.IsEmpty then
    Result := -7000
  else
    Result := ISO8601ToDate(s);
end;

function TdmJIRAAssistantController.IsStatusDone(AStatus: string): Boolean;
begin
  Result := MatchText(AStatus, ['������', '�������', '����������']);
end;

function TdmJIRAAssistantController.Login(ALogin, APassword: string): Boolean;
begin
  Auth.Username := ALogin;
  Auth.Password := APassword;
  RequestProject.Params.Clear;
  RequestProject.ResourceSuffix := 'project';
  RequestProject.Execute;
  Result := (ResponseProject.StatusCode = 200);// and ResponseDS.LocateEx('key;name', VarArrayOf(['FW', 'Freeway']), [lxoCaseInsensitive, lxoCheckOnly, lxoPartialKey]);
  if Result then
    memtblEmployee.Locate('Email', ALogin);
end;

function TdmJIRAAssistantController.BuildPeriodList: TArray<TPeriod>;
var
  dt, dtMin: TDateTime;
  i, n: Integer;
begin
  dtMin := EncodeDate(2018, MonthJuly, 1);
  dt := Today;
  SetLength(Result, 24);
  n := 0;
  for i := 0 to 23 do
  begin
    Result[i].FromDate := StartOfTheMonth(dt);
    Result[i].ToDate := EndOfTheMonth(dt);
    Result[i].PeriodName := FormatDateTime('mmmm yyyy', dt);
    Inc(n);
    dt := IncMonth(dt, -1);
    if CompareDateTime(dt, dtMin) = LessThanValue then
      Break;
  end;
  SetLength(Result, n);
end;

function TdmJIRAAssistantController.MatchEvaluator(const Match: TMatch): string;
var
  s: string;
  parts: TArray<string>;
begin
  s := Match.Value;
  Result := s;
  s := s.Replace('[', '').Replace(']', '');
  parts := s.Split(['|']);
  if Length(parts) >= 2 then
    Result := parts[0];
end;

procedure TdmJIRAAssistantController.UpdateWorklogs(AFromDate, AToDate: TDateTime);
var
  sFromDate, sToDate, s, sEmail, sAssigneeUser, sKey: string;
  jsonObj: Variant;
  Me: TdmJIRAAssistantController;
  iID, iCountWorklogs: Integer;
  iTotalWorklogs: Variant;
begin
  Me := Self;
  FillChar(FWorklogTotals, sizeof(FWorklogTotals), 0);
  sFromDate := DateToStr(AFromDate, FFS);
  sToDate := DateToStr(AToDate + 1, FFS);
  sEmail := memtblStaff.FieldByName('Email').AsString;
  sAssigneeUser := sEmail.Substring(0, sEmail.IndexOf('@'));
  RequestSearch.ResourceSuffix := 'search';
  RequestSearch.Params.Clear;
  RequestSearch.Params.AddItem('maxResults', '1000');
  RequestSearch.Params.AddItem('jql', Format('worklogAuthor=%s and workLogDate>=%s and workLogDate<%s and timespent>0', [sAssigneeUser, sFromDate, sToDate]));
  RequestSearch.Params.AddItem('fields', 'key,worklog,status');
  ResponseSearch.RootElement := 'issues';
  RequestSearch.Execute;
  jsonObj := uJSON.json_decode(ResponseSearch.Content);
  memtblWorklogs.DisableControls;
  try
    memtblWorklogs.Close;
    memtblWorklogs.Open;
    iID := 1;
    uVarObj.ForEach(jsonObj.issues, procedure (ANameOrIndex, issue: Variant; var ABreak: Boolean)
    var
      worklog: Variant;
    begin
      sKey := VarToStr(issue.key);
      worklog := issue.fields.worklog;
      iCountWorklogs := CountElements(worklog.worklogs);
      if iCountWorklogs > 0 then
        iTotalWorklogs := worklog.total
      else
        iTotalWorklogs := 0;
      if iCountWorklogs <> iTotalWorklogs then
      begin
        RequestSearch.Params.Clear;
        RequestSearch.ResourceSuffix := 'issue/' + sKey + '/worklog';
        RequestSearch.Execute;
        worklog := uJSON.json_decode(ResponseSearch.Content);
      end;
      uVarObj.ForEach(worklog.worklogs, procedure (ANameOrIndex, worklog: Variant; var ABreak: Boolean)
      var
        dtStarted: TDateTime;
        timeH: double;
        sWorklogName, sStatus: string;
      begin
        sKey := VarToStr(issue.key);
        dtStarted := InternalFormatDateTime(worklog.started);
        sWorklogName := worklog.updateAuthor.name;
        if InRange(dtStarted, AFromDate, AToDate) then
          if (not VarIsEmpty(worklog.updateAuthor.emailAddress) and SameText(worklog.updateAuthor.emailAddress, sEmail))
            or (not VarIsEmpty(worklog.updateAuthor.name) and SameText(worklog.updateAuthor.name, sAssigneeUser)) then
          begin
            memtblWorklogs.Append;
            ifldWorklogsID.Value := iID;
            Inc(iID);
            dtfMyWorklogs_started.Value := InternalFormatDateTime(worklog.started);
            sfldMyWorklogs_key.Value := AnsiString(sKey);
            s := VarToStr(worklog.comment);
            s := TRegEx.Replace(s, '(!(.*?)\|thumbnail!)', '');
            s := TRegEx.Replace(s, '/\[(.*?)\]/', MatchEvaluator);
            sfldMyWorklogs_comment.Value := AnsiString(s);
            timeH := worklog.timeSpentSeconds / 3600;
            ffldMyWorklogs_timespent.Value := StrToFloatDef(FormatFloat('0.00', timeH), timeH);
            sStatus := VarToStr(issue.fields.status.name);
            sfldMyWorklogsstatus.AsString := sStatus;
            ifldWorklogsEventType.Value := 0;
            ifldWorklogsEventOptions.Value := 2;
            sfldWorklogsEventCaption.AsString := Format('%s [%g] - %s', [sKey, timeH, s]);
            ifldWorklogsEventColor.Value := IfThen(IsStatusDone(sStatus), clLime, clBtnFace);
            memtblWorklogs.Post;
            Me.FWorklogTotals.Hours := Me.FWorklogTotals.Hours + timeH;
            Inc(Me.FWorklogTotals.Worklogs);
          end;
      end);
    end);
    FWorklogTotals.Days := FWorklogTotals.Hours / 8;
  finally
    memtblWorklogs.First;
    memtblWorklogs.EnableControls;
  end;
end;

type
  TDayWorklog = class(TCollectionItem)
  public
    FDate: TDateTime;
    FKeys: string;
    FWorks: string;
    FHours: string;
    FStatus: string;
    FTotal: double;
  end;

  TDayWorklogList = class(TCollection)
  protected
    procedure AddWorklog(dtfMyWorklogs_started: TDateTimeField; sfldMyWorklogs_key, sfldMyWorklogs_comment, sfldMyWorklogsstatus: TStringField; ffldMyWorklogs_timespent: TFloatField);
  end;

procedure TdmJIRAAssistantController.SaveWorklogToFile(AFile, APeriodName: string);
var
  sl: TStringList;
  bm: TBookmark;
  sStatus, sStyle: string;
begin
  sl := TStringList.Create;
  memtblWorklogs.DisableControls;
  bm := memtblWorklogs.Bookmark;
  try
    sl.Add('<html>');
    sl.Add('<header>');
    sl.Add('</header>');
    sl.Add('<body>');
    sl.AddStrings(['<h1>', APeriodName + ' - ' +  memtblStaff.FieldByName('Email').AsString, '</h1>']);
    with WorklogTotals do
      sl.AddStrings(['<h4>', Format('Total: days - %.2f; hours - %.2f; work-logs - %d', [Days, Hours, Worklogs]), '</h4>']);
    sl.Add('<table border=1>');
    sl.Add('<tr><th>Date</th><th>Task</th><th>Work</th><th>Time</th><th>Status</th></tr>');
    memtblWorklogs.First;
    while not memtblWorklogs.Eof do
    begin
      sl.Add('<tr>');
      sl.AddStrings(['<td>', dtfMyWorklogs_started.AsString, '</td>']);
      sl.AddStrings(['<td>', sfldMyWorklogs_key.AsString, '</td>']);
      sl.AddStrings(['<td>', sfldMyWorklogs_comment.AsString, '</td>']);
      sl.AddStrings(['<td>', ffldMyWorklogs_timespent.AsString, '</td>']);
      sStatus := sfldMyWorklogsstatus.AsString;
      if IsStatusDone(sStatus) then
        sStyle := 'style="color:green"'
      else if SameText(sStatus, '� ������') then
        sStyle := 'style="font-weight:bold"'
      else
        sStyle := '';
      sl.AddStrings(['<td ' + sStyle + '>', sStatus, '</td>']);
      sl.Add('</tr>');
      memtblWorklogs.Next;
    end;
    sl.Add('</table>');
    sl.Add('</body>');
    sl.Add('</html>');
    sl.SaveToFile(AFile);
  finally
    sl.Free;
    memtblWorklogs.Bookmark := bm;
    memtblWorklogs.EnableControls;
  end;
end;

procedure TDayWorklogList.AddWorklog(dtfMyWorklogs_started: TDateTimeField; sfldMyWorklogs_key, sfldMyWorklogs_comment, sfldMyWorklogsstatus: TStringField; ffldMyWorklogs_timespent: TFloatField);
var
  i: Integer;
  wl: TDayWorklog;
begin
  wl := nil;
  for i := 0 to Count - 1 do
    if TDayWorklog(Items[i]).FDate = dtfMyWorklogs_started.Value then
    begin
      wl := TDayWorklog(Items[i]);
      Break;
    end;
  if not Assigned(wl) then
    wl := TDayWorklog(inherited Add);
  wl.FDate := dtfMyWorklogs_started.Value;
  if not wl.FKeys.IsEmpty then
    wl.FKeys := wl.FKeys + '<br/>';
  wl.FKeys := wl.FKeys + Format('<a href="https://%s/browse/%s" target="_blank">%1:s</a>', ['freewayfleet.atlassian.net', sfldMyWorklogs_key.AsString]);
  if not wl.FWorks.IsEmpty then
    wl.FWorks := wl.FWorks + '<br/>';
  wl.FWorks := wl.FWorks + sfldMyWorklogs_comment.AsString;
  if not wl.FStatus.IsEmpty then
    wl.FStatus := wl.FStatus + '<br/>';
  wl.FStatus := wl.FStatus + sfldMyWorklogsstatus.AsString;
  if not wl.FHours.IsEmpty then
    wl.FHours := wl.FHours + '<br/>';
  wl.FHours := wl.FHours + ffldMyWorklogs_timespent.AsString;
  wl.FTotal := wl.FTotal + ffldMyWorklogs_timespent.Value;
end;

function TdmJIRAAssistantController.SaveWorklogToHTML: string;
var
  wlogs: TDayWorklogList;
  wl: TDayWorklog;
  sl: TStringList;
  bm: TBookmark;
  i: Integer;
begin
  wlogs := nil;
  sl := nil;
  memtblWorklogs.DisableControls;
  bm := memtblWorklogs.Bookmark;
  try
    wlogs := TDayWorklogList.Create(TDayWorklog);
    memtblWorklogs.First;
    while not memtblWorklogs.Eof do
    begin
      wlogs.AddWorklog(dtfMyWorklogs_started, sfldMyWorklogs_key, sfldMyWorklogs_comment, sfldMyWorklogsstatus, ffldMyWorklogs_timespent);
      memtblWorklogs.Next;
    end;

    sl := TStringList.Create;
    sl.Add('<html>');
    sl.Add('<header>');
    sl.Add('</header>');
    sl.Add('<body oncontextmenu="return false;">');
    with WorklogTotals do
      sl.AddStrings(['<h4>', Format('Total: days - %.2f; hours - %.2f; work-logs - %d', [Days, Hours, Worklogs]), '</h4>']);
    sl.Add('<table border=1>');
    sl.Add('<tr><th>Date</th><th>Task</th><th>Work</th><th>Time</th><th>Total per day</th></tr>');
    for i := 0 to wlogs.Count - 1 do
    begin
      wl := TDayWorklog(wlogs.Items[i]);
      sl.Add('<tr>');
      sl.AddStrings(['<td>', DateToStr(wl.FDate), '</td>']);
      sl.AddStrings(['<td>', wl.FKeys, '</td>']);
      sl.AddStrings(['<td>', wl.FWorks, '</td>']);
      sl.AddStrings(['<td align=right>', wl.FHours, '</td>']);
      if wl.FTotal < 8 then
        sl.AddStrings(['<td align=right bgcolor=red>', wl.FTotal.ToString, '</td>'])
      else
        sl.AddStrings(['<td align=right bgcolor=silver>', wl.FTotal.ToString, '</td>']);
      sl.Add('</tr>');
    end;
    sl.Add('</table>');
    sl.Add('</body>');
    sl.Add('</html>');
    Result := sl.Text;
  finally
    memtblWorklogs.Bookmark := bm;
    memtblWorklogs.EnableControls;
    sl.Free;
    wlogs.Free;
  end;
end;

end.

