program jiraAssistant;

uses
  Vcl.Forms,
  ja.MainForm in 'ja.MainForm.pas' {jaMainForm},
  ja.JIRAAssistantController in 'ja.JIRAAssistantController.pas' {dmJIRAAssistantController: TDataModule},
  uJSON in 'uJSON.pas',
  uVarObj in 'uVarObj.pas',
  ja.EventHintForm in 'ja.EventHintForm.pas' {jaEventHintForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'JIRA Assistant';
  Application.CreateForm(TdmJIRAAssistantController, dmJIRAAssistantController);
  Application.CreateForm(TjaMainForm, jaMainForm);
  Application.Run;
end.
