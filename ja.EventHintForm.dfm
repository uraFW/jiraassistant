inherited jaEventHintForm: TjaEventHintForm
  ClientHeight = 241
  ClientWidth = 482
  PixelsPerInch = 96
  TextHeight = 13
  inherited lcMain: TdxLayoutControl
    Width = 482
    Height = 241
    inherited pbEvent: TPaintBox
      Width = 462
    end
    inherited pbTaskComplete: TcxProgressBar
      Top = 200
      Width = 399
    end
    inherited liEventCaption: TdxLayoutItem
      ControlOptions.OriginalWidth = 200
    end
    inherited liStartCaption: TdxLayoutLabeledItem
      CaptionOptions.Text = 'Date:'
    end
    inherited liEndCaption: TdxLayoutLabeledItem
      CaptionOptions.Text = 'Work:'
    end
    inherited liEnd: TdxLayoutLabeledItem
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = True
      SizeOptions.SizableVert = True
      SizeOptions.Height = 49
      SizeOptions.Width = 418
      CaptionOptions.AlignVert = tavTop
      CaptionOptions.WordWrap = True
    end
    inherited liLocationCaption: TdxLayoutLabeledItem
      CaptionOptions.Text = 'Time:'
    end
    inherited liReminderCaption: TdxLayoutLabeledItem
      Visible = False
    end
    inherited liReminder: TdxLayoutLabeledItem
      Visible = False
    end
    inherited lgStartAndEnd: TdxLayoutGroup
      ItemIndex = 1
      Index = 3
    end
    inherited lgStartAndEndCaptions: TdxLayoutGroup
      ItemIndex = 1
    end
    inherited lgLocationAndReminder: TdxLayoutGroup
      Index = 5
    end
    inherited liTaskComplete: TdxLayoutItem
      Visible = False
      Index = 6
    end
    inherited lsiSpace2: TdxLayoutEmptySpaceItem
      Visible = False
      Index = 4
    end
    inherited lgLocation: TdxLayoutGroup
      Parent = lcMainGroup_Root
      Index = 2
    end
    inherited lgReminder: TdxLayoutGroup
      Index = 0
    end
  end
  inherited dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList
    inherited dxLayoutSkinLookAndFeel1: TdxLayoutSkinLookAndFeel
      PixelsPerInch = 96
    end
    inherited dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel
      PixelsPerInch = 96
    end
  end
end
