unit ja.EventHintForm;

interface

uses
  SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls,
  cxSchedulerEventModernInfoContainer, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxLayoutContainer, dxLayoutcxEditAdapters,
  cxClasses, dxLayoutLookAndFeels, cxProgressBar, dxLayoutControl;

type
  TjaEventHintForm = class(TcxSchedulerEventModernInfoContainer)
  protected
    procedure CheckCaptions; override;
    procedure CheckVisibility; override;
    procedure InitializeInfo; override;
  end;

var
  jaEventHintForm: TjaEventHintForm;

implementation

uses
  cxSchedulerStorage;

{$R *.dfm}

{ TForm1 }

procedure TjaEventHintForm.CheckCaptions;
begin
  inherited;
  liStartCaption.Caption := 'Date';
  liEndCaption.Caption := 'Work';
  liLocationCaption.Caption := 'Time';
end;

procedure TjaEventHintForm.CheckVisibility;
begin
  liStartCaption.Visible := True;
  liStart.Visible := True;
  liEndCaption.Visible := True;
  liEnd.Visible := True;
  lgStartAndEnd.Visible := True;
  lsiSpace1.Visible := False;
  lgLocation.Visible := True;
  lgReminder.Visible := False;
  liTaskComplete.Visible := False;
  lgLocationAndReminder.Visible := False;
  lsiSpace2.Visible := False;
end;

procedure TjaEventHintForm.InitializeInfo;
begin
  inherited;
  liStart.Caption := DateToStr(EventCell.Event.Start);
  liEnd.Caption := EventCell.Event.Message;
  liLocation.Caption := EventCell.Event.GetCustomFieldValueByIndex(0);
end;

initialization
  cxSchedulerEventModernInfoContainerClass := TjaEventHintForm;
end.
