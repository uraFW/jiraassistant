﻿unit uJSON;

interface

function json_decode(jsonText: string): Variant;
function json_encode(varObject: Variant): string;

implementation

uses
  Winapi.ActiveX, System.SysUtils, System.Classes, System.Variants, System.StrUtils, System.JSON, Data.DB, System.ZLib,
  uVarObj;

type
  TuJSONParser = class
  private
    FRootObject: TJSONObject;
    FRootVarObj: Variant;
    procedure DoJSONObject(varObj: Variant; jsonObj: TJSONObject);
    procedure DoJSONArray(varObj: Variant; jsonArrayName: string; jsonArray: TJSONArray);
    function JSONArrayToVar(jsonArray: TJSONArray): Variant;
    function StrToFloat(s: string; Def: Extended = 0.0): Extended;
  public
    constructor Create(jsonText: string = '');
    destructor Destroy; override;
    procedure DoParse(jsonText: string);
    property RootObject: Variant read FRootVarObj;
  end;

constructor TuJSONParser.Create(jsonText: string);
begin
  inherited Create;
  DoParse(jsonText);
end;

destructor TuJSONParser.Destroy;
begin
  FreeAndNil(FRootObject);
  inherited;
end;

procedure TuJSONParser.DoParse(jsonText: string);
begin
  FRootVarObj := uVarObj.CreateObject;
  FreeAndNil(FRootObject);
  if jsonText <> '' then
  begin
    FRootObject := TJSONObject.ParseJSONValue(jsonText) as TJSONObject;
    DoJSONObject(FRootVarObj, FRootObject);
  end;
end;

function TuJSONParser.StrToFloat(s: string; Def: Extended): Extended;
var
  fs: TFormatSettings;
begin
  if not TryStrToFloat(s, Result) then
  begin
    fs := TFormatSettings.Create;
    fs.DecimalSeparator := '.';
    if not TryStrToFloat(s, Result, fs) then
    begin
      fs.DecimalSeparator := ',';
      if not TryStrToFloat(s, Result, fs) then
        Result := Def;
    end;
  end;
end;

procedure TuJSONParser.DoJSONObject(varObj: Variant; jsonObj: TJSONObject);
var
  i: Integer;
  p: TJSONPair;
  NewObj, v: Variant;
  par: TParam;
begin
  if jsonObj <> nil then
    for i := 0 to jsonObj.Count - 1 do
    begin
      p := jsonObj.Pairs[i];
      if p.JsonValue is TJSONObject then
      begin
        NewObj := uVarObj.CreateObject;
        DoJSONObject(NewObj, p.JsonValue as TJSONObject);
        varObj.__SetProp(p.JsonString.Value, Null);
        par := ParamsOf(varObj).FindParam(p.JsonString.Value);
        par.Value := NewObj;
      end
      else if p.JsonValue is TJSONArray then
        DoJSONArray(varObj, p.JsonString.Value, p.JsonValue as TJSONArray)
      else
      begin
        varObj.__SetProp(p.JsonString.Value, Null);
        if p.JsonValue is TJSONTrue then
          v := True
        else if p.JsonValue is TJSONFalse then
          v := False
        else if p.JsonValue is TJSONNull then
          v := Null
        else if p.JsonValue is TJSONNumber then
          v := StrToFloat(p.JsonValue.Value)
        else
          v := p.JsonValue.Value;
        par := ParamsOf(varObj).FindParam(p.JsonString.Value);
        par.Value := v;
      end;
    end;
end;

procedure TuJSONParser.DoJSONArray(varObj: Variant; jsonArrayName: string; jsonArray: TJSONArray);
var
  i: Integer;
  v: TJSONValue;
  NewObj: Variant;
  p: TParam;
  vArr, vVal: Variant;
begin
  if jsonArray.Count > 0 then
    varObj.__SetProp(jsonArrayName, Null, jsonArray.Count - 1);
  for i := 0 to jsonArray.Count - 1 do
  begin
    v := jsonArray.Items[i];
    if v is TJSONObject then
    begin
      NewObj := uVarObj.CreateObject;
      DoJSONObject(NewObj, v as TJSONObject);
      varObj.__SetProp(jsonArrayName, Null, i);
      p := ParamsOf(varObj).FindParam(jsonArrayName);
      vArr := p.Value;
      VarArrayPut(vArr, NewObj, [i]);
      p.Value := vArr;
    end
    else if v is TJSONArray then
    begin
      NewObj := JSONArrayToVar(v as TJSONArray);
      varObj.__SetProp(jsonArrayName, Null, i);
      p := ParamsOf(varObj).FindParam(jsonArrayName);
      vArr := p.Value;
      VarArrayPut(vArr, NewObj, [i]);
      p.Value := vArr;
    end
    else
    begin
      varObj.__SetProp(jsonArrayName, Null, i);
      if v is TJSONTrue then
        vVal := True
      else if v is TJSONFalse then
        vVal := False
      else if v is TJSONNull then
        vVal := Null
      else if v is TJSONNumber then
        vVal := StrToFloat(v.Value)
      else
        vVal := v.Value;
      p := ParamsOf(varObj).FindParam(jsonArrayName);
      vArr := p.Value;
      VarArrayPut(vArr, vVal, [i]);
      p.Value := vArr;
    end;
  end;
end;

function TuJSONParser.JSONArrayToVar(jsonArray: TJSONArray): Variant;
var
  NewArr: array of Variant;
  i: Integer;
  v: TJSONValue;
  NewObj: Variant;
begin
  SetLength(NewArr, jsonArray.Count);
  for i := 0 to jsonArray.Count - 1 do
  begin
    v := jsonArray.Items[i];
    if v is TJSONObject then
    begin
      NewObj := uVarObj.CreateObject;
      NewArr[i] := NewObj;
      DoJSONObject(NewObj, v as TJSONObject);
    end
    else if v is TJSONArray then
    begin
      NewObj := JSONArrayToVar(v as TJSONArray);
      NewArr[i] := NewObj;
    end
    else
      NewArr[i] := TJSONString(v).Value;
  end;
  Result := NewArr;
end;

function json_decode(jsonText: string): Variant;
var
  JSONParser: TuJSONParser;
  bIsArray: Boolean;
begin
  Result := Unassigned;
  jsonText := Trim(jsonText);
  if jsonText = '' then
    exit;
  bIsArray := (jsonText[1] = '[') and (jsonText[Length(jsonText)] = ']');
  if bIsArray then
    jsonText := '{"__array":' + jsonText + '}'
  else if not((jsonText[1] = '{') and (jsonText[Length(jsonText)] = '}')) then
    exit(Null);
  Result := uVarObj.CreateObject;
  JSONParser := TuJSONParser.Create;
  try
    JSONParser.DoParse(jsonText);
    Result := JSONParser.RootObject;
    if bIsArray then
      Result := Result.__array;
  finally
    JSONParser.Free;
  end;
end;

function internal_encode_string(v: Variant): string;
var
  s: string;
  i: Integer;
  wCode: Word;
begin
  Result := '';
  s := StringReplace(VarToStr(v), '\', '\\', [rfReplaceAll, rfIgnoreCase]);
  for i := 1 to Length(s) do
  begin
    wCode := Ord(s[i]);
    if (wCode < 128) then
      Result := Result + s[i]
    else
      Result := Result + '\u' + IntToHex(wCode, 4);
  end;
  Result := StringReplace(Result, #13#10, '\n', [rfReplaceAll, rfIgnoreCase]);
end;

function internal_json_encode(varObject: Variant): TJSONValue;
var
  jsonRes, jsonVal: TJSONValue;
begin
  if uVarObj.IsValidObject(varObject) then
    Result := TJSONObject.Create
  else if VarIsArray(varObject) then
    Result := TJSONArray.Create
  else
    Result := TJSONNull.Create;
  jsonRes := Result;
  uVarObj.ForEach(varObject,
    procedure(ANameOrIndex, AValue: Variant; var ABreak: Boolean)
    begin
      if uVarObj.IsValidObject(AValue) or VarIsArray(AValue) then
      begin
        jsonVal := internal_json_encode(AValue);
        if jsonRes is TJSONObject then
          TJSONObject(jsonRes).AddPair(ANameOrIndex, jsonVal)
        else if jsonRes is TJSONArray then
          TJSONArray(jsonRes).AddElement(jsonVal);
      end
      else
      begin
        if jsonRes is TJSONObject then
          TJSONObject(jsonRes).AddPair(ANameOrIndex, internal_encode_string(AValue))
        else if jsonRes is TJSONArray then
          TJSONArray(jsonRes).Add(internal_encode_string(AValue));
      end;
    end);
end;

function json_encode(varObject: Variant): string;
var
  jsonObj: TJSONValue;
begin
  jsonObj := internal_json_encode(varObject);
  Result := jsonObj.ToString;
end;

end.
