unit ja.MainForm;

interface

uses
  ja.JIRAAssistantController,
  Types, Windows, Messages, ActiveX, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls, WinXCtrls, DB, Grids, DBGrids,
  ImageList, ImgList, Actions, ActnList, Menus, ToolWin, IniFiles, DBCtrls, StdActns, MSHTML,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxEdit, cxScheduler, cxSchedulerStorage, cxSchedulerCustomControls,
  cxSchedulerCustomResourceView, cxSchedulerDayView, cxSchedulerAgendaView, cxSchedulerDateNavigator, cxSchedulerHolidays, cxSchedulerTimeGridView, cxSchedulerUtils,
  cxSchedulerWeekView, cxSchedulerYearView, cxSchedulerGanttView, cxSchedulerRecurrence, dxBarBuiltInMenu, cxSchedulerRibbonStyleEventEditor, cxSchedulerTreeListBrowser,
  cxClasses, cxSchedulerDBStorage, Vcl.OleCtrls, SHDocVw;

type
  TDBGrid = class(DBGrids.TDBGrid)

  end;

  TjaMainForm = class(TForm)
    pnlLogin: TRelativePanel;
    edtLogin: TEdit;
    edtPassword: TEdit;
    btnConnect: TButton;
    pnlWork: TPanel;
    pgcWork: TPageControl;
    tsWorklogs: TTabSheet;
    tlbWorklogs: TToolBar;
    aclMyWorklogs: TActionList;
    ilJIRAAssistant: TImageList;
    actRefreshWorklogs: TAction;
    tbtnRefreshMyWorklogs: TToolButton;
    pnlWorklogsTools: TPanel;
    lblPeriod: TLabel;
    cbbPeriod: TComboBox;
    dbgrdWorklogs: TDBGrid;
    pnlWorklogsTotals: TPanel;
    chkRememberPassword: TCheckBox;
    lblEmployee: TLabel;
    dblkcbbEmployee: TDBLookupComboBox;
    actSave: TFileSaveAs;
    tbtnSave: TToolButton;
    pgcWorklogViews: TPageControl;
    tsSimpleGrid: TTabSheet;
    tsCalendar: TTabSheet;
    schMonth: TcxScheduler;
    schdbstgWorklogs: TcxSchedulerDBStorage;
    tsHTML: TTabSheet;
    wbWorklogs: TWebBrowser;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
    procedure OnLoginChanged(Sender: TObject);
    procedure actRefreshWorklogsExecute(Sender: TObject);
    procedure dbgrdWorklogsDrawDataCell(Sender: TObject; const Rect: TRect; Field: TField; State: TGridDrawState);
    procedure dbgrdWorklogsCellClick(Column: TColumn);
    procedure dbgrdWorklogsColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
    procedure dsEmployeeDataChange(Sender: TObject; Field: TField);
    procedure cbbPeriodChange(Sender: TObject);
    procedure actSaveUpdate(Sender: TObject);
    procedure actSaveAccept(Sender: TObject);
  private
    FPeriodList: TArray<TPeriod>;
    FKeyColIndex: Integer;
    FIni: TIniFile;
    FSavedLogin, FSavedPassword: string;
    FWebDocument: IHTMLDocument2;
    procedure WMSetCursor(var Msg: TWMSetCursor); message WM_SETCURSOR;
  end;

var
  jaMainForm: TjaMainForm;

implementation

uses
  UITypes, ShellAPI, IOUtils, NetEncoding, StrUtils;

{$R *.dfm}

type
  TDBGridFriend = class(TCustomDBGrid);

{ TjaMainForm }

procedure TjaMainForm.FormCreate(Sender: TObject);
var
  s: string;
  i: Integer;
begin
  schMonth.DateNavigator.Visible := False;
  pgcWorklogViews.Visible := False;
  pgcWorklogViews.ActivePageIndex := 0;
  s := TPath.GetHomePath + '\jiraAssistant\';
  ForceDirectories(s);
  FIni := TIniFile.Create(s + 'jiraAssistant.ini');
  Caption := Application.Title + ' - Login';
  FKeyColIndex := 3;
  SetBounds(0, 0, 0, 0);
  FPeriodList := dmJIRAAssistantController.BuildPeriodList;
  cbbPeriod.Items.BeginUpdate;
  cbbPeriod.Items.Clear;
  for i := Low(FPeriodList) to High(FPeriodList) do
    cbbPeriod.Items.Add(FPeriodList[i].Text);
  cbbPeriod.ItemIndex := 0;
  cbbPeriod.Items.EndUpdate;
  pnlWorklogsTotals.ShowCaption := False;
  FSavedLogin := FIni.ReadString('Account', 'Login', edtLogin.Text);
  FSavedPassword := TBase64Encoding.Base64.Decode(FIni.ReadString('Account', 'Password', TBase64Encoding.Base64.Encode(edtPassword.Text)));
  edtLogin.Text := FSavedLogin;
  edtPassword.Text := FSavedPassword;
  chkRememberPassword.Checked := not FSavedLogin.IsEmpty;
  wbWorklogs.Navigate('about:blank');
  FWebDocument := wbWorklogs.Document as IHtmlDocument2;
end;

procedure TjaMainForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FIni);
end;

procedure TjaMainForm.OnLoginChanged(Sender: TObject);
begin
  btnConnect.Enabled := (Trim(edtLogin.Text) <> '') and (Trim(edtPassword.Text) <> '');
end;

procedure TjaMainForm.actSaveAccept(Sender: TObject);
begin
  dmJIRAAssistantController.SaveWorklogToFile(actSave.Dialog.FileName, FPeriodList[cbbPeriod.ItemIndex].PeriodName);
  if MessageDlg('The Employee Work Log was saved successfully. Do you want to open it?', mtConfirmation, mbYesNo, 0) = mrYes then
    ShellExecute(0, 'open', PChar(actSave.Dialog.FileName), nil, nil, SW_SHOWMAXIMIZED);
end;

procedure TjaMainForm.actSaveUpdate(Sender: TObject);
begin
  actSave.Enabled := dmJIRAAssistantController.memtblWorklogs.Active;
end;

procedure TjaMainForm.btnConnectClick(Sender: TObject);
begin
  if dmJIRAAssistantController.Login(edtLogin.Text, edtPassword.Text) then
  begin
    if chkRememberPassword.Checked then
    begin
      FIni.WriteString('Account', 'Login', edtLogin.Text);
      FIni.WriteString('Account', 'Password', TBase64Encoding.Base64.Encode(edtPassword.Text));
    end
    else
    begin
      FIni.DeleteKey('Account', 'Login');
      FIni.DeleteKey('Account', 'Password');
    end;
    BorderStyle := bsSizeable;
    WindowState := wsMaximized;
    pnlLogin.Hide;
    Caption := Application.Title + ' - ' + edtLogin.Text;
    pnlWork.Show;
    actRefreshWorklogs.Execute;
    dmJIRAAssistantController.dsEmployee.OnDataChange := Self.dsEmployeeDataChange;
    dblkcbbEmployee.Enabled := MatchText(edtLogin.Text, ['sergey@obscuredev.net', 'iryna@obscuredev.net', 'ura@obscuredev.net']);
  end;
end;

procedure TjaMainForm.WMSetCursor(var Msg: TWMSetCursor);
var
  pt: TPoint;
  Cell: TGridCoord;
begin
  if pgcWorklogViews.ActivePage = tsSimpleGrid then
  begin
    pt := dbgrdWorklogs.ScreenToClient(Mouse.CursorPos);
    Cell := TDBGridFriend(dbgrdWorklogs).MouseCoord(pt.X, pt.Y);
    if (Cell.X = FKeyColIndex) and (Cell.Y > 1) and not dbgrdWorklogs.DataSource.DataSet.Eof then
    begin
      Windows.SetCursor(LoadCursor(0, IDC_HAND));
      Msg.Result := 1;
    end
    else
      inherited;
  end
  else
    inherited;
end;

procedure TjaMainForm.actRefreshWorklogsExecute(Sender: TObject);
var
  i: Integer;
  V: OleVariant;
begin
  Screen.Cursor := crHourGlass;
  try
    dmJIRAAssistantController.UpdateWorklogs(FPeriodList[cbbPeriod.ItemIndex].FromDate, FPeriodList[cbbPeriod.ItemIndex].ToDate);
    with dmJIRAAssistantController.WorklogTotals do
      pnlWorklogsTotals.Caption := Format('Total: days - %.2f; hours - %.2f; work-logs - %d', [Days, Hours, Worklogs]);
    pnlWorklogsTotals.ShowCaption := True;
    for i := 0 to dbgrdWorklogs.Columns.Count - 1 do
    begin
      dbgrdWorklogs.Columns[i].Title.Alignment := taCenter;
      if (i = 0) or (i > 5) then
        dbgrdWorklogs.Columns[i].Visible := False;
    end;
    pgcWorklogViews.Visible := True;
    schMonth.GoToDate(FPeriodList[cbbPeriod.ItemIndex].FromDate, vmMonth);
    V := VarArrayCreate([0, 0], varVariant);
    V[0] := dmJIRAAssistantController.SaveWorklogToHTML;
    FWebDocument.Write(PSafeArray(TVarData(v).VArray));
    FWebDocument.Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TjaMainForm.dbgrdWorklogsCellClick(Column: TColumn);
var
  pt: TPoint;
  Cell: TGridCoord;
begin
  pt := dbgrdWorklogs.ScreenToClient(Mouse.CursorPos);
  Cell := TDBGridFriend(dbgrdWorklogs).MouseCoord(pt.X, pt.Y);
  if (Cell.X = FKeyColIndex) and (Cell.Y > 1) and not dbgrdWorklogs.DataSource.DataSet.Eof then
    ShellExecute(0, 'open', PChar('https://freewayfleet.atlassian.net/browse/' + dmJIRAAssistantController.sfldMyWorklogs_key.AsString), nil, nil, SW_SHOWMAXIMIZED);
end;

procedure TjaMainForm.dbgrdWorklogsColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
begin
  if FromIndex = FKeyColIndex - 1 then
    FKeyColIndex := ToIndex + 1;
end;

procedure TjaMainForm.dbgrdWorklogsDrawDataCell(Sender: TObject; const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  if Field = dmJIRAAssistantController.sfldMyWorklogs_key then
  begin
    dbgrdWorklogs.Canvas.Font.Name := 'Times New Roman';
    dbgrdWorklogs.Canvas.Font.Style := [fsUnderline];
    dbgrdWorklogs.Canvas.Font.Color := clNavy;
    dbgrdWorklogs.Canvas.Font.Size := dbgrdWorklogs.Canvas.Font.Size + 1;
  end;
  if Field = dmJIRAAssistantController.sfldMyWorklogsstatus then
  begin
    if dmJIRAAssistantController.IsStatusDone(Field.Value) then
    begin
      dbgrdWorklogs.Canvas.Brush.Color := clGreen;
      dbgrdWorklogs.Canvas.Font.Color := clWhite;
      dbgrdWorklogs.Canvas.Font.Style := [fsBold];
    end;
  end;
  if gdSelected in State then
  begin
    with TDBGridFriend(dbgrdWorklogs) do
      DrawCellHighlight(Rect, State, Col, Row);
  end;
  dbgrdWorklogs.DefaultDrawDataCell(Rect, Field, State);
end;

procedure TjaMainForm.dsEmployeeDataChange(Sender: TObject; Field: TField);
begin
  cbbPeriodChange(Sender);
  actSave.Dialog.FileName :=  FormatDateTime('yymm', Date)
end;

procedure TjaMainForm.cbbPeriodChange(Sender: TObject);
begin
  dmJIRAAssistantController.memtblWorklogs.Close;
  pnlWorklogsTotals.ShowCaption := False;
  pgcWorklogViews.Visible := False;
end;

end.

